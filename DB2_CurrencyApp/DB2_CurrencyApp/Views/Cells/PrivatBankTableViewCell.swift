//
//  PrivatBankTableViewCell.swift
//  DB2_CurrencyApp
//
//  Created by Ivan Obodovskyi on 7/14/19.
//  Copyright © 2019 Ivan Obodovskyi. All rights reserved.
//

import UIKit

class PrivatBankTableViewCell: UITableViewCell {
  @IBOutlet weak var currencyName: UILabel!
  @IBOutlet weak var buyPriceLabel: UILabel!
  @IBOutlet weak var sellPriceLabel: UILabel!
  
    override func awakeFromNib() {
        super.awakeFromNib()
      
    }
  
  func setUpCell(name: String, buy: String, sell: String) {
    currencyName.text = name
    buyPriceLabel.text = buy
    sellPriceLabel.text = sell
  }

}
