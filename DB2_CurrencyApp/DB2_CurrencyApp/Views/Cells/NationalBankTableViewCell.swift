//
//  NationalBankTableViewCell.swift
//  DB2_CurrencyApp
//
//  Created by Ivan Obodovskyi on 7/14/19.
//  Copyright © 2019 Ivan Obodovskyi. All rights reserved.
//

import UIKit

class NationalBankTableViewCell: UITableViewCell {
  @IBOutlet weak var currencyNameLabel: UILabel!
  @IBOutlet weak var currencyPriceLabel: UILabel!
  @IBOutlet weak var currencyEquivalentLabel: UILabel!
  

  public func setUpCell(name: String, price: String, equivalent: String) {
    currencyNameLabel.text = name
    currencyPriceLabel.text = price
    currencyEquivalentLabel.text = equivalent
  }
}
