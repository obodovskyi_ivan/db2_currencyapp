//
//  MainScreenViewController.swift
//  DB2_CurrencyApp
//
//  Created by Ivan Obodovskyi on 7/13/19.
//  Copyright © 2019 Ivan Obodovskyi. All rights reserved.
//

import UIKit

class MainScreenViewController: UIViewController {
  
  @IBOutlet weak var privatTableView: UITableView!
  @IBOutlet weak var nationalBankTableView: UITableView!
  
  private var networkRequest = NetworkRequest()
  
  private(set) var privatBankData: PrivatBankModel?
  
  private var privatRates: [ExchangeRate] = [] {
    didSet {
      DispatchQueue.main.async {
        self.privatTableView.reloadData()
      }
    }
  }
  
  private(set) var nationalBankData: [NationalBankData]! {
    didSet {
      DispatchQueue.main.async {
        self.nationalBankTableView.reloadData()
      }
    }
  }
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    networkRequest.fetchData(url: NetworkEndpoints.nationalBankCurrencyOnDate + NetworkEndpoints.jsonExtension, completion: serializeNationalBanktData(requestData:))
    print(NetworkEndpoints.nationalBankCurrencyOnDate + NetworkEndpoints.jsonExtension)
    
    networkRequest.fetchData(url: NetworkEndpoints.privatBankCurrencyOnDate, completion: serializePrivatData(requestData:))
    setUpTableViews()
  }
  
  private func setUpTableViews() {
    privatTableView.delegate = self
    privatTableView.dataSource = self
    nationalBankTableView.delegate = self
    nationalBankTableView.dataSource = self
  }
}

// MARK: - Serialization functions
extension MainScreenViewController {
  private func serializePrivatData(requestData: (Data)) {
    do {
      self.privatBankData = try JSONDecoder().decode(PrivatBankModel.self, from: requestData)
      self.privatBankData?.exchangeRate.map({
        if $0.purchaseRate > 0 {
          switch $0.currency {
          case "USD" :
            privatRates.insert($0, at: 0)
          case "EUR":
            privatRates.insert($0, at: 1)
          default:
            privatRates.append($0)
          }
        }
      })
    } catch let error {
      print(error.localizedDescription)
    }
  }
  
  private func serializeNationalBanktData(requestData: (Data)) {
    do {
      self.nationalBankData = try JSONDecoder().decode([NationalBankData].self, from: requestData)
    } catch let error {
      print(error.localizedDescription)
    }
  }
  
}

// MARK: - TableView DataSource
extension MainScreenViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    var numberOfRows: Int!
    if tableView == privatTableView {
      numberOfRows = privatRates.count
    } else if tableView == nationalBankTableView {
      guard let num = self.nationalBankData?.count else {return 0}
      numberOfRows = num
    }
    
    return numberOfRows
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    switch tableView {
    case privatTableView:
      guard let cell = privatTableView.dequeueReusableCell(withIdentifier: PrivatBankTableViewCell.identifier, for: indexPath) as? PrivatBankTableViewCell else { return UITableViewCell() }
      
      let buyPrice: String = self.moveDecimalPoint(num: self.privatRates[indexPath.row].purchaseRate)
      let sellPrice = self.moveDecimalPoint(num: self.privatRates[indexPath.row].saleRate)
      
      cell.setUpCell(name: privatRates[indexPath.row].currency, buy: buyPrice, sell: sellPrice)
      
      return cell
    case nationalBankTableView:
      guard let cell = nationalBankTableView.dequeueReusableCell(withIdentifier: NationalBankTableViewCell.identifier, for: indexPath) as? NationalBankTableViewCell else { return UITableViewCell() }
      
      let price = self.moveDecimalPoint(num: self.nationalBankData[indexPath.row].rate) + " UAH"
      let equivalent = "1 \(self.nationalBankData[indexPath.row].cc)"
      
      cell.setUpCell(name: self.nationalBankData[indexPath.row].txt, price: price, equivalent: equivalent)
      return cell
    default:
      return UITableViewCell()
    }
  }
  
  
}

// MARK: - TableView Delegate
extension MainScreenViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    switch tableView {
    case privatTableView:
      
      let pickedCell = privatTableView.cellForRow(at: indexPath) as! PrivatBankTableViewCell
      let itemToScroll = pickedCell.currencyName.text ?? ""
      let index = nationalBankData.firstIndex { (item) -> Bool in
        item.cc == itemToScroll
      }
      if let searchIndex = index {
        moveToSelectedItem(for: nationalBankTableView, as: searchIndex, animated: true, position: .top)
      } else {
        buildAlert(for: pickedCell)
      }
      
    case nationalBankTableView:
      let pickedCell = nationalBankTableView.cellForRow(at: indexPath) as! NationalBankTableViewCell
      
      //Searches the currency value by three last characters of equivalent label
      let itemToFind = pickedCell.currencyEquivalentLabel.text ?? ""
      let searchIndex = itemToFind.index(itemToFind.endIndex, offsetBy: -3)
      let searchValue = itemToFind[searchIndex...]
      
      if let indexPathToSearch = privatRates.firstIndex(where: { (item) -> Bool in
        item.currency == searchValue
      }) {
        moveToSelectedItem(for: privatTableView, as: indexPathToSearch, animated: true, position: .top)
      } else {
        buildAlert(for: pickedCell)
      }
      
    default:
      return
    }
  }
  
  private func moveToSelectedItem(for tableView: UITableView, as searchIndex: Array<Any>.Index, animated: Bool, position: UITableView.ScrollPosition) {
    let indexPath = IndexPath(item: searchIndex, section: 0)
    
    tableView.scrollToRow(at: indexPath, at: position, animated: animated)
    tableView.selectRow(at: indexPath, animated: animated, scrollPosition: position)
    
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
      tableView.deselectRow(at: indexPath, animated: animated)
    }
  }
  
  private func buildAlert(for cell: UITableViewCell) {
    let message = AlertMessages().alertMessageBuild(cell: cell)
    _ = AlertManager().showNoDataAtCellAlert(on: self, title: "No Data!", message: message, style: .alert)
  }
}


