//
//  NetworkService.swift
//  DB2_CurrencyApp
//
//  Created by Ivan Obodovskyi on 7/14/19.
//  Copyright © 2019 Ivan Obodovskyi. All rights reserved.
//

import Foundation

class NetworkRequest {
  
  
  public func performRequest(url: String, completion: @escaping (Result<Data, Error>) -> Void)  {
    
    guard let url = URL(string: url) else {
      print("Wrong url")
      return
    }
    print(url)
    let urlSessionTask = URLSession.shared.dataTask(with: url) { data, response, error in
      if let error = error {
        completion(.failure(error))
      }  else if let data = data {
        completion(.success(data))
        print(data)
      } else {
        print("NoData")
      }
    }
    urlSessionTask.resume()
  }
  
  public func fetchData(url: String, completion: @escaping (Data) -> () ) {
    performRequest(url: url) { (result) in
      switch result {
      case .failure(let error):
        print(error.localizedDescription)
      case .success(let data):
        completion(data)
      }
    }
  }

  
}
