//
//  Double+FloatingPoint.swift
//  DB2_CurrencyApp
//
//  Created by Ivan Obodovskyi on 7/14/19.
//  Copyright © 2019 Ivan Obodovskyi. All rights reserved.
//

import Foundation

extension MainScreenViewController {
  func moveDecimalPoint(num: Float?) -> String {
    guard let num = num else { return "N/A" }
    return String(format: "%.2f", num)
  }
}
