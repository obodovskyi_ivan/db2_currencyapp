//
//  UITableViewCell+Identifier.swift
//  DB2_CurrencyApp
//
//  Created by Ivan Obodovskyi on 7/14/19.
//  Copyright © 2019 Ivan Obodovskyi. All rights reserved.
//

import UIKit


extension UITableViewCell {
  
  static var identifier: String {
    return String(describing: self)
  }
  
  static var nib: UINib {
    return UINib(nibName: identifier, bundle: nil)
  }
}
