//
//  PrivatBankModel.swift
//  DB2_CurrencyApp
//
//  Created by Ivan Obodovskyi on 7/13/19.
//  Copyright © 2019 Ivan Obodovskyi. All rights reserved.
//

import Foundation


struct PrivatBankModel: Codable {
  let date: String
  var exchangeRate: [ExchangeRate]
  
  
  enum CodingKeys: String, CodingKey {
    case date
    case exchangeRate
  }
  
  func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    try container.encode(date, forKey: .date)
    try container.encode(exchangeRate, forKey: .exchangeRate)
  }
  
  init() {
    self.date = ""
    self.exchangeRate = []
  }
  
  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    date = try container.decodeIfPresent(String.self, forKey: .date) ?? ""
    exchangeRate = try container.decodeIfPresent(Array.self, forKey: .exchangeRate) ?? []
  }
}


struct ExchangeRate: Codable {
  let currency: String
  let saleRate: Float
  let purchaseRate: Float
  
  enum CodingKeys: String, CodingKey {
    case currency
    case saleRate
    case purchaseRate
  }
  
  func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    try container.encode(currency, forKey: .currency)
    try container.encode(saleRate, forKey: .saleRate)
    try container.encode(purchaseRate, forKey: .purchaseRate)
  }
  
  init() {
    self.currency = ""
    self.saleRate = 0.0
    self.purchaseRate = 0.0
  }
  
  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    currency = try container.decodeIfPresent(String.self, forKey: .currency) ?? ""
    saleRate = try container.decodeIfPresent(Float.self, forKey: .saleRate) ?? 0
    purchaseRate = try container.decodeIfPresent(Float.self, forKey: .purchaseRate) ?? 0
  }
}
