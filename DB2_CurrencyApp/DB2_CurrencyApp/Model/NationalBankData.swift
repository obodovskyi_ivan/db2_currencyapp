//
//  NationalBankModel.swift
//  DB2_CurrencyApp
//
//  Created by Ivan Obodovskyi on 7/13/19.
//  Copyright © 2019 Ivan Obodovskyi. All rights reserved.
//

import Foundation
typealias NBUData = [NationalBankData]


struct NationalBankData: Codable {
  var txt: String
  var rate: Float
  var cc: String
  
  enum CodingKeys: String, CodingKey {
    case txt = "txt"
    case cc = "cc"
    case rate = "rate"
  }
  
  func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    try container.encode(txt, forKey: .txt)
    try container.encode(rate, forKey: .rate)
    try container.encode(cc, forKey: .cc)
  }
  
  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    txt = try container.decodeIfPresent(String.self, forKey: .txt) ?? ""
    rate = try container.decodeIfPresent(Float.self, forKey: .rate) ?? 0
    cc = try container.decodeIfPresent(String.self, forKey: .cc) ?? ""
  }
}
