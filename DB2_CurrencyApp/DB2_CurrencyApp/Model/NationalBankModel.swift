//
//  NationalBankModel.swift
//  DB2_CurrencyApp
//
//  Created by Ivan Obodovskyi on 7/13/19.
//  Copyright © 2019 Ivan Obodovskyi. All rights reserved.
//

import Foundation

struct NationalBankModel: Codable {
  var txt: String
  var cc: String
  var rate: Float
  
  enum CodingKeys: String, CodingKey {
    case txt, cc
    case rate
  }
  
  func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    try container.encode(txt, forKey: .txt)
    try container.encode(cc, forKey: .cc)
    try container.encode(rate, forKey: .rate)
  }
  
  init() {
    self.txt = ""
    self.cc = ""
    self.rate = 0
  }
  
  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    txt = try container.decodeIfPresent(String.self, forKey: .txt) ?? ""
    cc = try container.decodeIfPresent(String.self, forKey: .cc) ?? ""
    rate = try container.decodeIfPresent(Float.self, forKey: .rate) ?? 0
    
  }
}
