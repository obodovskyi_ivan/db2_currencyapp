//
//  AlertManager.swift
//  DB2_CurrencyApp
//
//  Created by Ivan Obodovskyi on 7/15/19.
//  Copyright © 2019 Ivan Obodovskyi. All rights reserved.
//

import UIKit

struct AlertMessages {
  static let noDataAtCellTitle: String = "No data found!"
  
  public func alertMessageBuild(cell: UITableViewCell) -> String {
    switch cell {
    case is PrivatBankTableViewCell:
      return "No data found at National Bank Database"
    default:
      return "No data found at PrivatBank Database"
    }
  }
}


class AlertManager {
  public func showNoDataAtCellAlert(on viewController: UIViewController, title: String, message: String, style: UIAlertController.Style) {
    let controller = UIAlertController(title: title, message: message, preferredStyle: style)
    let alertAction = UIAlertAction(title: "OK!", style: .cancel, handler: nil)
    
    controller.addAction(alertAction)
    viewController.present(controller, animated: true)
  }
}
