//
//  NetworkEndpoints.swift
//  DB2_CurrencyApp
//
//  Created by Ivan Obodovskyi on 7/13/19.
//  Copyright © 2019 Ivan Obodovskyi. All rights reserved.
//

import Foundation


struct NetworkEndpoints {
  //Main bank APIs' for getting data by date
  //Date format for NBU data = 20190711 + jsonExtension
  static var nationalBankCurrencyOnDate: String = "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?date=20190711"
  //Privat date format date = 01.12.2014
  static var privatBankCurrencyOnDate: String = "https://api.privatbank.ua/p24api/exchange_rates?json&date=11.07.2019"
  
  //Json path for the NBU API link
  static var jsonExtension: String = "&json"
}
